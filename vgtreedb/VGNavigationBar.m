//
//  VGNavigationBar.m
//  vgtreedb
//
//  Created by Steven Kim on 1/27/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGNavigationBar.h"

@implementation VGNavigationBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect{
    UIImage *image = [UIImage imageNamed: @"VGNavigationBar.png"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

@end

//
//  VGAppDelegate.h
//  vgtreedb
//
//  Created by Steven Kim on 1/22/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VGApi.h"

@interface VGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) VGApi *vgapi;

@end

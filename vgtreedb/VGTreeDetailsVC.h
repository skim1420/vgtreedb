//
//  VGTreeDetailsVC.h
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VGApi.h"

@interface VGTreeDetailsVC : UIViewController

@property (strong, nonatomic) VGApi *vgapi;
@property (strong, nonatomic) NSDictionary *tree;

@end

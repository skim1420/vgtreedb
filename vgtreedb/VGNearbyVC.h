//
//  VGNearbyVC.h
//  vgtreedb
//
//  Created by Steven Kim on 1/27/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VGApi.h"

@interface VGNearbyVC : UIViewController

@property (strong, nonatomic) VGApi *vgapi;
@property (strong, nonatomic) UIButton *menuBtn;

@end

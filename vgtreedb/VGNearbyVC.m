//
//  VGNearbyVC.m
//  vgtreedb
//
//  Created by Steven Kim on 1/27/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGNearbyVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "VGAppDelegate.h"
#import "ECSlidingViewController.h"
#import "VGMenuTVC.h"

@interface VGNearbyVC () <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *map;

@property (strong, nonatomic) NSArray *trees;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *myLocation;

@end

@implementation VGNearbyVC

@synthesize menuBtn = _menuBtn;

@synthesize vgapi = _vgapi;
@synthesize trees = _trees;
@synthesize locationManager = _locationManager;
@synthesize myLocation = _myLocation;

#pragma mark - Other methods

- (void)displayTrees {

    self.trees = [self.vgapi getNearbyTrees:self.myLocation];
    NSLog(@"nearby trees: %@", self.trees);

    self.map.region = MKCoordinateRegionMakeWithDistance(self.myLocation.coordinate, 20, 20);
    self.map.centerCoordinate = self.myLocation.coordinate;
    
    for (NSDictionary *tree in self.trees) {

        CLLocationCoordinate2D coord;
        coord.latitude = [[tree valueForKey:@"latitude"] doubleValue];
        coord.longitude = [[tree valueForKey:@"longitude"] doubleValue];

        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        annotationPoint.coordinate = coord;
        annotationPoint.title = [tree valueForKey:@"common"];
        annotationPoint.subtitle = [NSString stringWithFormat:@"#%@",[tree valueForKey:@"site"]];
        
        [self.map addAnnotation:annotationPoint];
    }
}

- (void)revealMenu {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

#pragma mark - View management

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.384 green:0.506 blue:0.208 alpha:1.000];

    // load the model in
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.vgapi = appDelegate.vgapi;
    
    [self.map setUserTrackingMode:MKUserTrackingModeFollowWithHeading];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
    
    // Sliding menu
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[VGMenuTVC class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.menuBtn.frame = CGRectMake(8, 10, 34, 24);
    [self.menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [self.menuBtn addTarget:self action:@selector(revealMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.menuBtn];
    self.navigationItem.leftBarButtonItem = menuBarButton;
}

- (void)viewDidUnload {
    [self setMap:nil];
    [super viewDidUnload];
}

#pragma mark - Location Manager delegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [self.locationManager stopUpdatingLocation];

    self.myLocation = newLocation;
    [self displayTrees];
}

@end

//
//  VGInitVC.m
//  vgtreedb
//
//  Created by Steven Kim on 2/16/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGInitVC.h"

@interface VGInitVC ()

@end

@implementation VGInitVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Court"];
}

@end

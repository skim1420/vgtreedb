//
//  VGTreeTypesTVC.h
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VGTreeTypesTVC : UITableViewController

@property (strong, nonatomic) UIButton *menuBtn;

@end

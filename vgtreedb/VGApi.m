//
//  VGApi.m
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGApi.h"
#import "NSString+URLEncoding.h"

#define apikey @"vgtreedb"

@implementation VGApi

- (NSArray *)getCourts {

    NSArray *courts = [self vgTreeDbApi:@"method=getCourts&status=closed"];
    return courts;
}

- (NSArray *)getCommons {
    
    NSArray *commons = [self vgTreeDbApi:@"method=getCommons&status=closed"];
    return commons;
}

- (NSArray *)getTreesForCourt:(NSString *)court {
    
    NSArray *trees = [self vgTreeDbApi:[NSString stringWithFormat:@"method=getTrees&status=closed&court=%@", [court encodedURLString]]];

    // sort by tree type
    NSMutableArray *treeSorter = [trees mutableCopy];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"common"  ascending:YES];
    [treeSorter sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    trees = [treeSorter copy];

    return trees;
}

- (NSArray *)getTreesForCommon:(NSString *)common {
    
    NSArray *trees = [self vgTreeDbApi:[NSString stringWithFormat:@"method=getTrees&status=closed&common=%@", [common encodedURLString]]];
    
    // sort by tree type
    NSMutableArray *treeSorter = [trees mutableCopy];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"common"  ascending:YES];
    [treeSorter sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    trees = [treeSorter copy];
    
    return trees;
}

- (NSArray *)getNearbyTrees:(CLLocation *)coord {

    NSArray *trees = [self vgTreeDbApi:[NSString stringWithFormat:@"method=getNearbyTrees&status=closed&latitude=%f&longitude=%f", coord.coordinate.latitude, coord.coordinate.longitude]];
    return trees;
}

#pragma mark - Utility

- (NSArray *)vgTreeDbApi:(NSString *)queryParam {
    
    NSArray *returnData = nil;
    
    NSString *urlString = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/trees.php?%@", queryParam];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:apikey forHTTPHeaderField:@"Authorization"];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];

    if (responseData != nil) {
        NSLog(@"request: %@\nresponse: %i\n%@", urlString, response.statusCode, [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding]);
        returnData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    }
    
    return returnData;
}

@end

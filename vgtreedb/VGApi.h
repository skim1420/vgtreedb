//
//  VGApi.h
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface VGApi : NSObject

- (NSArray *)getCourts;
- (NSArray *)getCommons;
- (NSArray *)getTreesForCourt:(NSString *)court;
- (NSArray *)getTreesForCommon:(NSString *)common;
- (NSArray *)getNearbyTrees:(CLLocation *)coord;

@end

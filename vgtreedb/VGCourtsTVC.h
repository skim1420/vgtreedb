//
//  VGCourtsTVC.h
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface VGCourtsTVC : UITableViewController

@property (strong, nonatomic) UIButton *menuBtn;

@end

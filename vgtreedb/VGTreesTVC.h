//
//  VGTreesTVC.h
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VGApi.h"

@interface VGTreesTVC : UITableViewController

@property (strong, nonatomic) NSString *court;
@property (strong, nonatomic) NSString *common;
@property (strong, nonatomic) VGApi *vgapi;

@end

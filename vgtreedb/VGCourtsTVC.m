//
//  VGCourtsTVC.m
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGCourtsTVC.h"
#import "VGApi.h"
#import "VGTreesTVC.h"
#import "VGAppDelegate.h"
#import "ECSlidingViewController.h"
#import "VGMenuTVC.h"

@interface VGCourtsTVC ()

@property (strong, nonatomic) VGApi *vgapi;
@property (strong, nonatomic) NSArray *courts;

@property (nonatomic, strong) NSMutableDictionary *thumbnailsCache;
@property (assign) BOOL runQ;

@end

@implementation VGCourtsTVC

@synthesize menuBtn = _menuBtn;

@synthesize vgapi = _vgapi;
@synthesize courts = _courts;

@synthesize thumbnailsCache = _thumbnailsCache;
@synthesize runQ;

#pragma mark - Other methods

- (NSMutableDictionary *)thumbnailsCache{
    if (!_thumbnailsCache) {
        _thumbnailsCache = [[NSMutableDictionary alloc] init];
    }
    return _thumbnailsCache;
}

- (void)addToThumbnailsCache:(NSData *)newObject withKey:(NSString *)newKey{
    [self.thumbnailsCache setObject:newObject forKey:newKey];
}

- (void)revealMenu {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

#pragma mark - View management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.384 green:0.506 blue:0.208 alpha:1.000];
    
    // load the model in
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.vgapi = appDelegate.vgapi;
    
    // Sliding menu
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[VGMenuTVC class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.menuBtn.frame = CGRectMake(8, 10, 34, 24);
    [self.menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [self.menuBtn addTarget:self action:@selector(revealMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.menuBtn];
    self.navigationItem.leftBarButtonItem = menuBarButton;
}

- (void)viewWillAppear:(BOOL)animated {
    
    // flag for run operation on GCD queue
    runQ = YES;

    self.courts = [self.vgapi getCourts];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    // flush operations on GCD queue
    runQ = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Segue Trees"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSString *court = [[self.courts objectAtIndex:indexPath.row] valueForKey:@"court"];
        [segue.destinationViewController setCourt:court];
        [segue.destinationViewController setVgapi:self.vgapi];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.courts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Court Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Court Cell"];
    }
    // cleanse cell of all imageviews
    for (UIView *view in cell.subviews) {
        if ([view class] == [UIImageView class]) {
            [view removeFromSuperview];
        }
    }
    
    NSDictionary *court = [self.courts objectAtIndex:indexPath.row];
    NSString *courtId = [court valueForKey:@"thumb_id"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,60,60)];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setClipsToBounds:YES];
    [cell addSubview:imageView];
    
    NSString *courtName = [court valueForKey:@"court"];
    if ([courtName isEqualToString:@""]) {
        courtName = @"(Unspecified)";
    }
    cell.textLabel.text = courtName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ trees", [court valueForKey:@"count"]];
    
    if (![[self.thumbnailsCache allKeys] containsObject:courtId]) {
        
        // cache miss, get it from server
        NSString *pictureUrl = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/photos/%i_thumb.jpg", [courtId intValue]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (runQ) {
                NSData *pictureData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:pictureUrl]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (pictureData != nil) {
                        UIImage *realImage = [[UIImage alloc] initWithData:pictureData];
                        [imageView setImage:realImage];
                    }
                });
                if (pictureData != nil) {
                    [self addToThumbnailsCache:pictureData withKey:courtId];
                }
            }
        });

    } else {
        
        // cache hit
        NSData *image = [self.thumbnailsCache objectForKey:courtId];
        [imageView setImage:[UIImage imageWithData:image]];

    }

    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
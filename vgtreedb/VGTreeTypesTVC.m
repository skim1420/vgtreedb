//
//  VGTreeTypesTVC.m
//  vgtreedb
//
//  Created by Steven Kim on 1/25/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGTreeTypesTVC.h"
#import "VGApi.h"
#import "VGTreesTVC.h"
#import "VGAppDelegate.h"
#import "ECSlidingViewController.h"
#import "VGMenuTVC.h"

@interface VGTreeTypesTVC ()

@property (strong, nonatomic) VGApi *vgapi;
@property (strong, nonatomic) NSArray *treeTypes;

@property (nonatomic, strong) NSMutableDictionary *thumbnailsCache;
@property (assign) BOOL runQ;

@end

@implementation VGTreeTypesTVC

@synthesize menuBtn = _menuBtn;

@synthesize vgapi = _vgapi;
@synthesize treeTypes = _treeTypes;

@synthesize thumbnailsCache = _thumbnailsCache;
@synthesize runQ;

#pragma mark - Other methods

- (NSMutableDictionary *)thumbnailsCache{
    if (!_thumbnailsCache) {
        _thumbnailsCache = [[NSMutableDictionary alloc] init];
    }
    return _thumbnailsCache;
}

- (void)addToThumbnailsCache:(NSData *)newObject withKey:(NSString *)newKey{
    [self.thumbnailsCache setObject:newObject forKey:newKey];
}

- (void)revealMenu {
    [self.slidingViewController anchorTopViewTo:ECRight];
}

#pragma mark - View management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.384 green:0.506 blue:0.208 alpha:1.000];

    // load the model in
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.vgapi = appDelegate.vgapi;
    
    // Sliding menu
    self.navigationController.view.layer.shadowOpacity = 0.75f;
    self.navigationController.view.layer.shadowRadius = 10.0f;
    self.navigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[VGMenuTVC class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
    self.menuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.menuBtn.frame = CGRectMake(8, 10, 34, 24);
    [self.menuBtn setBackgroundImage:[UIImage imageNamed:@"menuButton.png"] forState:UIControlStateNormal];
    [self.menuBtn addTarget:self action:@selector(revealMenu) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:self.menuBtn];
    self.navigationItem.leftBarButtonItem = menuBarButton;
}

- (void)viewWillAppear:(BOOL)animated {
    
    // flag for run operation on GCD queue
    runQ = YES;

    self.treeTypes = [self.vgapi getCommons];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    // flush operations on GCD queue
    runQ = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Segue Trees"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSString *common = [[self.treeTypes objectAtIndex:indexPath.row] valueForKey:@"common"];
        [segue.destinationViewController setCommon:common];
        [segue.destinationViewController setVgapi:self.vgapi];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.treeTypes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tree Type Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Tree Type Cell"];
    }
    // cleanse cell of all imageviews
    for (UIView *view in cell.subviews) {
        if ([view class] == [UIImageView class]) {
            [view removeFromSuperview];
        }
    }
    
    NSDictionary *treeType = [self.treeTypes objectAtIndex:indexPath.row];
    NSString *treeTypeId = [treeType valueForKey:@"thumb_id"];

    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,60,60)];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setClipsToBounds:YES];
    [cell addSubview:imageView];
    
    NSString *treeTypeName = [treeType valueForKey:@"common"];
    if ([treeTypeName isEqualToString:@""]) {
        treeTypeName = @"(Unspecified)";
    }
    cell.textLabel.text = treeTypeName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ trees", [treeType valueForKey:@"count"]];
    
    if (![[self.thumbnailsCache allKeys] containsObject:treeTypeId]) {
        
        // cache miss, get it from server
        NSString *pictureUrl = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/photos/%i_thumb.jpg", [treeTypeId intValue]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (runQ) {
                NSData *pictureData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:pictureUrl]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (pictureData != nil) {
                        UIImage *realImage = [[UIImage alloc] initWithData:pictureData];
                        [imageView setImage:realImage];
                    }
                });
                if (pictureData != nil) {
                    [self addToThumbnailsCache:pictureData withKey:treeTypeId];
                }
            }
        });

    } else {
        
        // cache hit
        NSData *image = [self.thumbnailsCache objectForKey:treeTypeId];
        [imageView setImage:[UIImage imageWithData:image]];
        
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
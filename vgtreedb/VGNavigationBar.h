//
//  VGNavigationBar.h
//  vgtreedb
//
//  Created by Steven Kim on 1/27/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VGNavigationBar : UINavigationBar

@end

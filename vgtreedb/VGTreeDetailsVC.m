//
//  VGTreeDetailsVC.m
//  vgtreedbinput
//
//  Created by Steven Kim on 1/24/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGTreeDetailsVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface VGTreeDetailsVC () <UIScrollViewDelegate>

@property (strong, nonatomic)  UIScrollView *scrollView;
@property (strong, nonatomic) UIView *informationView;

@property (strong, nonatomic) UILabel *siteLabel;
@property (strong, nonatomic) UILabel *courtLabel;
@property (strong, nonatomic) UIImageView *pictureIV;
@property (strong, nonatomic) MKMapView *map;

@end

@implementation VGTreeDetailsVC

@synthesize vgapi = _vgapi;
@synthesize tree = _tree;

@synthesize scrollView = _scrollView;
@synthesize informationView = _informationView;

@synthesize siteLabel = _siteLabel;
@synthesize courtLabel = _courtLabel;
@synthesize pictureIV = _pictureIV;
@synthesize map = _map;

#pragma mark - Other methods



#pragma mark - View management

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSLog(@"tree: %@", self.tree);

    self.title = [self.tree valueForKey:@"common"];

    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, -20, 320, 480)];
    self.scrollView.contentSize = CGSizeMake(320, 500);
    self.scrollView.delegate = self;
    [self.view addSubview:self.scrollView];
    
    self.pictureIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 240)];
    self.pictureIV.backgroundColor = [UIColor grayColor];
    [self.pictureIV setContentMode:UIViewContentModeScaleAspectFill];
    [self.pictureIV setClipsToBounds:YES];
    [self.scrollView addSubview:self.pictureIV];
    
    self.informationView = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 320, 260)];
    [self.scrollView addSubview:self.informationView];

    // site & court
    self.siteLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, 20)];
    self.siteLabel.backgroundColor = [UIColor whiteColor];
    self.siteLabel.textColor = [UIColor blackColor];
    self.siteLabel.text = [NSString stringWithFormat:@"Site #%i", [[self.tree valueForKey:@"site"] intValue] ];
    [self.informationView addSubview:self.siteLabel];
    
    self.courtLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 0, 200, 20)];
    self.courtLabel.backgroundColor = [UIColor whiteColor];
    self.courtLabel.textColor = [UIColor blackColor];
    self.courtLabel.textAlignment = UITextAlignmentRight;
    self.courtLabel.text = [self.tree valueForKey:@"court"];
    [self.informationView addSubview:self.courtLabel];
    
    // load picture
    NSString *pictureUrl = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/photos/%i.jpg", [[self.tree valueForKey:@"tree_id"] intValue]];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *pictureData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:pictureUrl]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (pictureData != nil) {
                UIImage *realImage = [[UIImage alloc] initWithData:pictureData];
                [self.pictureIV setImage:realImage];
            }
        });
    });

    // map
    self.map = [[MKMapView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 240)];
    self.map.mapType = MKMapTypeStandard;
    CLLocationCoordinate2D coord;
    coord.latitude = [[self.tree objectForKey:@"latitude"] doubleValue];
    coord.longitude = [[self.tree objectForKey:@"longitude"] doubleValue];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 100, 100);
    self.map.region = region;
    self.map.centerCoordinate = coord;
    [self.informationView addSubview:self.map];
    
    // drop a pin
    MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
    pin.coordinate = coord;
    [self.map addAnnotation:pin];

}

- (void)viewDidUnload {
    [self setMap:nil];
    [super viewDidUnload];
}
@end

//
//  VGTreesTVC.m
//  vgtreedbinput
//
//  Created by Steven Kim on 1/24/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGTreesTVC.h"
#import "VGTreeDetailsVC.h"

#define GROUP_COURT 1
#define GROUP_COMMON 2

@interface VGTreesTVC ()

@property (strong, nonatomic) NSArray *trees;
@property (assign) int groupingType;
@property (nonatomic, strong) NSMutableDictionary *thumbnailsCache;
@property (assign) BOOL runQ;

@end

@implementation VGTreesTVC

@synthesize court = _court;
@synthesize vgapi = _vgapi;
@synthesize trees = _trees;
@synthesize groupingType = _groupingType;
@synthesize thumbnailsCache = _thumbnailsCache;
@synthesize runQ;

#pragma mark - Other methods

- (NSMutableDictionary *)thumbnailsCache{
    if (!_thumbnailsCache) {
        _thumbnailsCache = [[NSMutableDictionary alloc] init];
    }
    return _thumbnailsCache;
}

- (void)addToThumbnailsCache:(NSData *)newObject withKey:(NSString *)newKey{
    [self.thumbnailsCache setObject:newObject forKey:newKey];
}

#pragma mark - View management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.court) {
        self.groupingType = GROUP_COURT;
        self.title = self.court;
        self.trees = [self.vgapi getTreesForCourt:self.court];
    } else if (self.common) {
        self.groupingType = GROUP_COMMON;
        self.title = self.common;
        self.trees = [self.vgapi getTreesForCommon:self.common];
    }
    NSLog(@"trees: %@", self.trees);
}

- (void)viewWillAppear:(BOOL)animated {
    
    // flag for run operation on GCD queue
    runQ = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    // flush operations on GCD queue
    runQ = NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Segue Tree Details"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        VGTreeDetailsVC *destVC = segue.destinationViewController;
        [destVC setVgapi:self.vgapi];
        [destVC setTree:[self.trees objectAtIndex:indexPath.row]];
        destVC.hidesBottomBarWhenPushed = YES;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.trees count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Tree Cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Tree Cell"];
    }
    // cleanse cell of all imageviews
    for (UIView *view in cell.subviews) {
        if ([view class] == [UIImageView class]) {
            [view removeFromSuperview];
        }
    }
    
    NSDictionary *tree = [self.trees objectAtIndex:indexPath.row];
    NSString *treeId = [tree valueForKey:@"tree_id"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,60,60)];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [imageView setClipsToBounds:YES];
    [cell addSubview:imageView];
    
    cell.textLabel.text = [tree valueForKey:@"common"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ #%@", [tree valueForKey:@"court"], [tree valueForKey:@"site"]];
    
    if (![[self.thumbnailsCache allKeys] containsObject:treeId]) {
        
        // cache miss, get it from server
        NSString *pictureUrl = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/photos/%i_thumb.jpg", [treeId intValue]];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (runQ) {
                NSData *pictureData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:pictureUrl]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (pictureData != nil) {
                        UIImage *realImage = [[UIImage alloc] initWithData:pictureData];
                        [imageView setImage:realImage];
                    }
                });
                if (pictureData != nil) {
                    [self addToThumbnailsCache:pictureData withKey:treeId];
                }
            }
        });

    } else {

        // cache hit
        NSData *image = [self.thumbnailsCache objectForKey:treeId];
        [imageView setImage:[UIImage imageWithData:image]];

    }
    
    return cell;

}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end

rm -r iTunesArtwork
rm -r Icon.png
rm -r Icon@2x.png
rm -r Icon-72.png
rm -r Icon-72@2x.png
rm -r Icon-Small.png
rm -r Icon-Small@2x.png
rm -r Icon-Small-50.png
rm -r Icon-Small-50@2x.png

cp base.png iTunesArtwork
sips --resampleWidth 57 base.png --out Icon.png
sips --resampleWidth 114 base.png --out Icon@2x.png
sips --resampleWidth 72 base.png --out Icon-72.png
sips --resampleWidth 144 base.png --out Icon-72@2x.png
sips --resampleWidth 29 base.png --out Icon-Small.png
sips --resampleWidth 58 base.png --out Icon-Small@2x.png
sips --resampleWidth 50 base.png --out Icon-Small-50.png
sips --resampleWidth 100 base.png --out Icon-Small-50@2x.png
